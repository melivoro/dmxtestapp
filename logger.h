#ifndef LOGGER_H
#define LOGGER_H

#include <QObject>
#include <QDateTime>
#include <QDebug>

class Logger : public QObject
{
    Q_OBJECT
public:
    static Logger *instance();

signals:
    void log(QString str);
    void logProcessed(QString str);

private slots:
    void onLog(QString str);

private:
    explicit Logger(QObject *parent = nullptr);
};

#if defined ENABLE_LOGS
    #define LOG(STR) emit Logger::instance()->log(QString(STR)); \
        qDebug() << QString("%1 %2").arg(QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss.zzz")).arg(STR);
#else
    #define LOG(STR)
#endif

#endif // LOGGER_H
