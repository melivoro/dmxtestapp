#ifndef PROJECTMANAGER_H
#define PROJECTMANAGER_H

#include <QFile>
#include <QTimer>
#include "DMXPlugin.h"

class ProjectManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString initStatus MEMBER m_initStatus NOTIFY initStatusChanged)
    Q_PROPERTY(QString openOutputStatus MEMBER m_openOutputStatus NOTIFY openOutputStatusChanged)
    Q_PROPERTY(QString closeOutputStatus MEMBER m_closeOutputStatus NOTIFY closeOutputStatusChanged)
    Q_PROPERTY(QString logString MEMBER m_logString NOTIFY logStringChanged)
public:
    explicit ProjectManager(QObject *parent = nullptr);
    Q_INVOKABLE void clearLog();
    Q_INVOKABLE void saveLog(QUrl fileName);

public slots:
    void onInit();
    void onOpenOutput(int device);
    void onSendDmxPkg(int device);
    void onBeginSendDmxPkg(int device);
    void onEndSendDmxPkg();
    void onCloseOutput(int device);

signals:
    void initStatusChanged();
    void checkDeviceExistStatusChanged();
    void checkOutputStatusChanged();
    void openOutputStatusChanged();
    void closeOutputStatusChanged();
    void logStringChanged();

private slots:
    void onInitFinished();
#if defined ENABLE_LOGS
    void onLog(QString str);
#endif
    void onTimeout();

private:
    QDmxUsbPlugin* m_plug = NULL;
    QString m_initStatus;
    QString m_openOutputStatus;
    QString m_closeOutputStatus;
    QString m_logString;
    QTimer m_timer;
    int m_currentDevice;
};

#endif // PROJECTMANAGER_H
