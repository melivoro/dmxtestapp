#include "projectmanager.h"
#include "logger.h"

ProjectManager::ProjectManager(QObject *parent) : QObject(parent)
{
    m_plug = new QDmxUsbPlugin(this);
    m_timer.setInterval(30);
#if defined ENABLE_LOGS
    connect(Logger::instance(), &Logger::logProcessed, this, &ProjectManager::onLog);
#endif
    connect(m_plug, &QDmxUsbPlugin::rescanDevicesFinished, this, &ProjectManager::onInitFinished);
    connect(&m_timer, &QTimer::timeout, this, &ProjectManager::onTimeout);
}

void ProjectManager::clearLog()
{
    m_logString.clear();
    emit logStringChanged();
}

void ProjectManager::saveLog(QUrl fileName)
{
    QString fileNameStr = fileName.path().remove(0, 1);
    QFile file(fileNameStr);
    if(file.open(QIODevice::WriteOnly | QIODevice::Text)) {
         QTextStream stream(&file);
         stream << m_logString;
         file.close();
    } else {
        LOG(QString("ProjectManager::saveLog(%1): cannot write file").arg(fileNameStr));
    }
}

void ProjectManager::onInit()
{
    LOG("ProjectManager::onInit()");
    if(m_plug == NULL) {
        return;
    }
    m_plug->init();
}

void ProjectManager::onInitFinished()
{
    LOG("ProjectManager::onInitFinished()");
    if(m_plug == NULL) {
        return;
    }
    auto devices = m_plug->getDevices();
    m_initStatus = tr("%1 devices found").arg(devices.size());
    emit initStatusChanged();
}

#if defined ENABLE_LOGS
void ProjectManager::onLog(QString str)
{
    m_logString.append(str);
    m_logString.append('\n');
    emit logStringChanged();
}
#endif

void ProjectManager::onTimeout()
{
    onSendDmxPkg(m_currentDevice);
}

void ProjectManager::onOpenOutput(int device)
{
    LOG(QString("ProjectManager::onOpenOutput(%1)").arg(device));
    if(m_plug == NULL) {
        return;
    }
    bool status = m_plug->openOutput(device, 0);
    LOG(QString("ProjectManager::onOpenOutput(%1): openOutput status: %2").arg(device).arg(status));
    if(status) {
        m_openOutputStatus = tr("output 0 opened on device %1").arg(device);
    } else {
        m_openOutputStatus = tr("output 0 not opened on device %1").arg(device);
    }
    emit openOutputStatusChanged();
}

void ProjectManager::onSendDmxPkg(int device)
{
    LOG(QString("ProjectManager::onSendDmxPkg(%1)").arg(device));
    if(m_plug == NULL) {
        return;
    }
    QByteArray data;
    data.resize(512);
    data.fill(0, 512);
    m_plug->writeDmx(device, 0, data);
}

void ProjectManager::onBeginSendDmxPkg(int device)
{
    m_currentDevice = device;
    m_timer.start();
}

void ProjectManager::onEndSendDmxPkg()
{
    m_timer.stop();
}

void ProjectManager::onCloseOutput(int device)
{
    LOG(QString("ProjectManager::onCloseOutput(%1)").arg(device));
    if(m_plug == NULL) {
        return;
    }
    bool status = m_plug->closeOutput(device, 0);
    LOG(QString("ProjectManager::onCloseOutput(%1): %2").arg(device).arg(status));
    if(status) {
        m_closeOutputStatus = tr("output 0 closed on device %1").arg(device);
    } else {
        m_closeOutputStatus = tr("failed to close output 0 on device %1").arg(device);
    }
    emit closeOutputStatusChanged();
}
