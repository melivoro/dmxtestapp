#include "logger.h"

Logger::Logger(QObject *parent) : QObject(parent)
{
    connect(this, &Logger::log, this, &Logger::onLog);
}

Logger *Logger::instance()
{
    static Logger inst;
    return &inst;
}

void Logger::onLog(QString str)
{
    emit logProcessed(QString("%1 %2").arg(QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm:ss.zzz")).arg(str));
}

