QT += quick
CONFIG += c++11
LIBS += -L$$PWD/ -lftd2xx
QMAKE_LFLAGS += /NODEFAULTLIB:LIBCMT
#DEFINES += ENABLE_LOGS

SOURCES += \
        DMXDevice.cpp \
        DMXPlugin.cpp \
        logger.cpp \
        main.cpp \
        projectmanager.cpp

HEADERS += \
    DMXDevice.h \
    DMXPlugin.h \
    ftd2xx.h \
    logger.h \
    projectmanager.h

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
