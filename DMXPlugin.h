#ifndef DMXPLUGIN_H
#define DMXPLUGIN_H

#include <QObject>
#include "DMXDevice.h"

class QDmxUsbPlugin : public QObject
{
    Q_OBJECT

public:
    QDmxUsbPlugin(QObject* parent = 0);
    ~QDmxUsbPlugin();
    QString errorString();
    bool outputIsOpened(quint32 device, quint32 port) const;
    virtual void init();

public slots:
    virtual void rescanDevices();
public:
    virtual QMap<quint32, DMXDevice*> getDevices() {return m_deviceList;}

signals:
    void rescanDevicesFinished();

public:
    virtual bool openOutput(quint32 device, quint32 port);
    virtual bool closeOutput(quint32 device, quint32 port);

public slots:
    virtual void writeDmx(quint32 device, quint32 port, QByteArray data);

private:
    QMap<quint32, DMXDevice*> m_deviceList;
    QMultiHash<quint32,quint32> m_openedOutput;
    QString m_lastError;
};

#endif // DMXPLUGIN_H
